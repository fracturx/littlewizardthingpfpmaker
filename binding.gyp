{
  "targets": [
    {
      "target_name": "littlewizardthingpfpmaker",
      "sources": [ "<!@(node -p \"require('fs').readdirSync('./addons').map(f=>'addons/'+f).join(' ')\")" ],
      "cflags!": [ "-fno-exceptions", "<!(pkg-config --cflags Magick++)" ],
      "cflags_cc": [ "-std=c++17" ],
      "cflags_cc!": [ "-fno-exceptions", "<!(pkg-config --cflags Magick++)" ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "<!@(pkg-config --cflags-only-I Magick++ | sed 's/^.\{2\}//')"
      ],
      'dependencies': ["<!(node -p \"require('node-addon-api').gyp\")"],
      "libraries": [
        "<!(pkg-config --libs Magick++)",
      ],
      "defines": ["NAPI_CPP_EXCEPTIONS", "MAGICKCORE_HDRI_ENABLE=false", "MAGICKCORE_QUANTUM_DEPTH=16"]
    }
  ]
}