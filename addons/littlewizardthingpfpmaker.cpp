#include <napi.h>
#include <Magick++.h>

using namespace std;
using namespace Napi;
using namespace Magick;

Napi::Value color(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    Napi::Buffer<char> image_data = info[0].As<Napi::Buffer<char>>();
    string mask_color = info[1].As<Napi::String>().Utf8Value();

    Blob finalBlob;

    Image image(Blob(image_data.Data(), image_data.Length()));

    size_t width = image.baseColumns();
    size_t height = image.baseRows();
    string dimensions = to_string(width) + "x" + to_string(height);

    // why does this work???
    Image mask(Geometry(dimensions), Color(mask_color.c_str()));

    mask.composite(image, Magick::CenterGravity, Magick::AtopCompositeOp);
    mask.magick("PNG");
    mask.write(&finalBlob);

    Object result_image = Object::New(env);
    result_image.Set("data", Napi::Buffer<char>::Copy(env, (char *)finalBlob.data(), finalBlob.length()));

    return result_image;
}

Napi::Object Init(Env env, Object exports) {
    exports.Set(String::New(env, "color"), Function::New(env, color));
    return exports;
}

NODE_API_MODULE(Addon, Init)